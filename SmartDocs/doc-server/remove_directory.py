# Remove all directory 30 min from now

# Get all directory, make a for loop, any thing larger than 30*60 seconds, delete it

import os

from pathlib import Path
from shutil import rmtree

import datetime

from glob import glob
dirs = glob("/mnt/docs/processing/saved/*")

now = datetime.datetime.now()

for direct in dirs:
    dir = direct.replace("/mnt/docs/processing/saved/","")
    time_in_sec = int(dir.split("_")[0])
    timestamp = datetime.datetime.fromtimestamp(time_in_sec)
    delta_sec = (now - timestamp).seconds
    if(delta_sec > 60*30):
        rmtree(direct)
        print(dir)
        print(delta_sec)

dirs = glob("/mnt/docs/processing/temp/*")

now = datetime.datetime.now()

for direct in dirs:
    dir = direct.replace("/mnt/docs/processing/temp/","")
    time_in_sec = int(dir.split("_")[0])
    timestamp = datetime.datetime.fromtimestamp(time_in_sec)
    delta_sec = (now - timestamp).seconds
    if(delta_sec > 60*5):
        rmtree(direct)
        print(dir)
        print(delta_sec)
