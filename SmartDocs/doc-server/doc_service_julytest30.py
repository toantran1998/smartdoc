#coding=utf-8
from flask import Flask, url_for
from flask import request
from flask import json
from flask import Response
from flask import jsonify
from gevent.pywsgi import WSGIServer
import requests

from PIL import Image
import cv2
import shutil

from scipy.ndimage import interpolation as inter
import pytesseract
from pytesseract import image_to_string, image_to_boxes
from pytesseract import Output
from pdf2image import convert_from_path
import img2pdf
from PyPDF2 import PdfFileWriter, PdfFileReader

from scipy import ndimage
import numpy as np

import io
import os
os.environ['OMP_THREAD_LIMIT'] = '1'
from os import listdir
from os.path import isfile, join
import sys
import re

import unidecode
import ntpath
from shutil import rmtree
from pathlib import Path
import json
import time
import datetime

import concurrent.futures
from fuzzysearch import find_near_matches

import string
import random

import multiprocessing as mp
from io import BytesIO

def rotateMulti(params):
	fname = params[0]
	print(fname)
	fname_saved = params[1]
	image = cv2.imread(fname)
	cv2.imwrite(fname_saved,image)
	try:
		image = Image.open(fname)
		angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
		rotated = image
		if angle != 0 and angle != 360:
			print(angle)
			rotated  = ndimage.rotate(image, angle)
		cv2.imwrite(fname,rotated)
		cv2.imwrite(fname_saved,rotated)
		return angle
	except Exception as e:
		print(e, flush=True)
		return 0

def sendImageBasedClassify(params):
	label = "unk"
	try:
		url = "http://10.32.32.31:3002/recognize"
		files = {'image': (params[0], open(params[1], 'rb')),}
		payload = {'department' : params[2]}
		response = requests.post(url, files = files, data = payload)
		label = response.json()["details"][0]["label"]
		if(label == "blank"):
			label = "unk"
		print(label,flush=True)
	except Exception as e:
		print(e, flush=True)
		label = "unrecognize"
	return label

def sendTesseractBasedClassify(params):
	label = params[0]
	fname = params[1]
	department = params[2]
	name_send = params[3]
	i = params[4]
	if(label == "unrecognize"):
		try:
			print("call Hung", flush=True)
			#local to sever
			url_ = "http://localhost:8889/api/v1/recognize/text"
			custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1 -l vie'
			text = ""
			host_tess = random.choice(host_tess_list)
			url = "http://"+ host_tess +":2210/tess_string"
			files = {'image': (name_send, open(fname, 'rb')),}
			payload = {'config' : custom_config}
			try:
				response = requests.post(url, files = files, data = payload)
				text = response.json()["data"]
				print(text)
				# print(response.json(),flush=True)
				# text = pytesseract.image_to_string(Image.open(fname),config=custom_config, lang='vie')
				# print(text)
			except Exception as e:
				print("tesseract error: " + str(e), flush=True)
			payload = {'department': department, 'text': text}
			response = requests.post(url_, json = payload)
			print(response.json(), flush=True)
			label = response.json()['data']['type']
			lines = text.split('\n')
			json_obj = get_doc_type()
			db_department = ""
			for d in json_obj:
				types[d['name']] = d['keywords']
				if(d['name'] == label):
					db_department = d['department']['code'].lower()
			keywords = types[label]
			res = {}
			res["content"] = text
			tags = {}
			for keyword in keywords:
				if 'TEXT_IN_BOX' in keyword['data_Type'] or 'ACC' in keyword["name"]:
					# print(f'{keyword["description"]}: ' + get_account_number(lines, keyword['match_Phrase']))
					tags[keyword["name"]] = get_account_number(lines, keyword['match_Phrase'])
					if len(tags[keyword["name"]]) < 9:
						start = text.lower().find('026c')
						tags[keyword["name"]] = text[start:start + 10]
					tags[keyword["name"]] = tags[keyword["name"]][:10].upper()

					print("Text in box, remove contour", flush=True)

					if 'TEXT_IN_BOX' in keyword['data_Type']:
						try:
							image = cv2.imread(fname)
							height, width, channels = (0,0,0)
							try:
								height, width, channels = image.shape
							except Exception as e:
								try:
									print("Shape Exception")
									height, width = image.shape
								except Exception as e:
									print("Still Shape Exception")
							half_image = 0
							if width > height:
								image = cv2.resize(image, (0,0), fx=2480/width, fy=1750/height)
								half_image = 1
							else:
								image = cv2.resize(image, (0,0), fx=2480/width, fy=3500/height)
							if half_image:
								print("half_image")
								image = rm_contour(image,5,6,10)
							else:
								image = rm_contour(image,6,8,10)
							cv2.imwrite(fname,image)
						except Exception as e:
							print(e, flush=True)

						# move remove contour here
						print("tesseracting")
						custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1 -l fdm-v12'
						text_acc = ""
						account_num = ""
						host_tess = random.choice(host_tess_list)
						url = "http://"+ host_tess +":2210/tess_string"
						files = {'image': (name_send, open(fname, 'rb')),}
						payload = {'config' : custom_config}
						try:
							response = requests.post(url, files = files, data = payload)
							text_acc = response.json()["data"].lower().replace(" ","")
							# text_acc = pytesseract.image_to_string(Image.open(fname),config=custom_config).lower().replace(" ","")
							start = text_acc.lower().find('026c')
							# print(text_acc)
							if(start > 0):
								account_num = text_acc[start:start + 10]
								account_num = ''.join(e for e in account_num if e.isalnum())
								print(account_num, flush=True)
						except Exception as e:
							print("tesseract error: " + str(e), flush=True)
						tags[keyword["name"]] = account_num.upper()
						tags[keyword["name"]] = tags[keyword["name"]][:10]
				elif keyword['data_Type'] == 'NAME':
					# print(f'{keyword["description"]}: ' + get_name(lines, keyword['match_Phrase']))
					tags[keyword["name"]] = get_name(lines, keyword['match_Phrase'])
				elif keyword['data_Type'] == 'NUMBER':
					# print(f'{keyword["description"]}: ' + get_number(lines, keyword['match_Phrase'], 9, 20))
					tags[keyword["name"]] = get_number(lines, keyword['match_Phrase'], 9, 20)
				elif keyword['data_Type'] == 'ADDRESS':
					if keyword['name'] == 'ID_LOCATION':
						# print(f'{keyword["description"]}: ' + get_id_location(lines, keyword['match_Phrase']))
						tags[keyword["name"]] = get_id_location(lines, keyword['match_Phrase'])
					else:
						# print(f'{keyword["description"]}: ' + get_address(lines, keyword['match_Phrase']))
						tags[keyword["name"]] = get_address(lines, keyword['match_Phrase'])
				elif keyword['data_Type'] == 'DATE':
					# print(f'{keyword["description"]}: ' + get_date(lines, keyword['match_Phrase']))
					tags[keyword["name"]] = get_date(lines, keyword['match_Phrase'])
				elif keyword['data_Type'] == 'EMAIL':
					# print(f'{keyword["description"]}: ' + get_email(lines, keyword['match_Phrase']))
					tags[keyword["name"]] = get_email(lines, keyword['match_Phrase'])

			params = (('input', text),)
			response = requests.get('http://localhost:9494/api/custom', params=params)
			res["tags_ner_custom"] = response.json()
			params = (('input', text),)
			response = requests.get('http://localhost:9494/api/based', params=params)
			res["tags_ner_base"] = response.json()
			res["tags"] = tags
			if ( label in ner_custom_whitelist):
				res["tags"] = res["tags_ner_custom"]

			if (department == "ktnb"):
				res["tags"] = res["tags_ner_base"]

			return (label,i,res)
		except Exception as e:
			print(e, flush=True)
			return (label,i,{})
	return (label,i,{})

def fast_extract_bin_pdf(byteIO, outfile):
	byteIO.seek(0)
	pdf = byteIO.read()
	startmark = b"\xff\xd8"
	startfix = 0
	endmark = b"\xff\xd9"
	endfix = 2
	i = 0
	njpg = 0
	while True:
		istream = pdf.find(b"stream", i)
		if istream < 0:
			break
		istart = pdf.find(startmark, istream, istream + 20)
		if istart < 0:
			i = istream + 20
			continue
		iend = pdf.find(b"endstream", istart)
		if iend < 0:
			raise Exception("Didn't find end of stream!")
		iend = pdf.find(endmark, iend - 20)
		if iend < 0:
			raise Exception("Didn't find end of JPG!")
		istart += startfix
		iend += endfix
		jpg = pdf[istart:iend]
		with open(outfile, "wb") as jpgfile:
			jpgfile.write(jpg)
			njpg += 1
			break
		i = iend
	return njpg

def fast_extract_image_pdf(fname, outfile):
	outfile = outfile + "/prefix"
	pageCount = 1
	with open(fname, 'rb') as infile:
		reader = PdfFileReader(infile)
		for page in range(reader.getNumPages()):
			writer = PdfFileWriter()
			writer.addPage(reader.getPage(page))
			with BytesIO() as out:
				writer.write(out)
				pageCount += fast_extract_bin_pdf(out, outfile+"-"+str(page+1)+".jpg")
	return pageCount


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))


def get_match_line(lines, signal, min, dist=1):
	if signal == None:
		return ''
	signal = unidecode.unidecode(signal).lower()
	data = ''
	for line in lines:
		line_decode = unidecode.unidecode(line).lower()
		match = find_near_matches(signal, line_decode, max_l_dist=dist)
		if len(match) > 0:
			data = line[match[0].end:]
			if len(data) >= min:
				break
	if len(data) < min:
		data = ''

	return data

def get_account_number(lines, signal):
	data = get_match_line(lines, signal, 10, dist=5)
	last_char = ''
	result = ''
	for i in range(0, len(data)):
		this_char = data[i]
		if this_char == 'c' or this_char == 'C':
			result += this_char
		elif this_char.isdigit():
			result += this_char
		last_char = this_char
	return result
	data = get_match_line(lines, signal, 9)
	return data

def get_number(lines, signal, min, max):
	data = get_match_line(lines, signal, min)
	x = re.search(rf"\b[\d-]{{{min},{max}}}", data)
	if x != None:
		return x.group()
	return ''

def get_name(lines, signal):
	data = get_match_line(lines, signal, 6)
	x = re.search(r"\b[\w\s]{5,60}(?=[ ]{2}|$)", data)
	if x != None:
		return x.group()
	return ''

def get_date(lines, signal):
	data = get_match_line(lines, signal, min=10)
	decode_data = unidecode.unidecode(data)
	x = re.search(r"\b(?:(\d{2})/(\d{2})/(\d{4}))|(?:(\d{2})\sthang\s(\d{2})\snam\s(\d{4}))", decode_data)
	if x != None:
		if x.group(1) != None:
			return f'{x.group(1)}/{x.group(2)}/{x.group(3)}'
		else:
			return f'{x.group(4)}/{x.group(5)}/{x.group(6)}'
	return ''

def get_address(lines, signal):
	data = get_match_line(lines, signal, 6)
	decode_data = unidecode.unidecode(data)
	x = re.search(r"\b[\w][\w/,\s]{4,}[\w](?=[ ]{2}|$)", decode_data)
	if x != None:
		return data[x.span()[0]:x.span()[1]].strip(',.-| ')
	return ''

def get_id_location(lines, signal):
	data = get_match_line(lines, signal, 6)
	decode_data = get_alphanumeric(unidecode.unidecode(data), repl=' ')
	x = re.search(r"\S+(\s\S+)*", decode_data)
	if x != None:
		return data[x.span()[0]:x.span()[1]]
	return ''

def get_email(lines, signal):
	data = get_match_line(lines, signal, 9)
	data = get_alphanumeric(unidecode.unidecode(data), '@.')
	x = re.search(r"\S+@\S+", data)
	if x != None:
		return data[x.span()[0]:x.span()[1]]
	return ''

def get_alphanumeric(raw, esc = None, repl = None):
	alphanumeric = ""
	for character in raw:
		if character.isalnum() or (esc != None and character in esc):
			alphanumeric += character
		elif repl != None:
			alphanumeric += repl
	return alphanumeric

def rotate(image, center = None, scale = 1):
	angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
	rotated = image
	if angle != 0 and angle != 360:
		rotated  = ndimage.rotate(image, angle)
	return rotated

def rm_contour(image,kernel_size,draw_size,iterations):
	gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
	horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size,1))
	detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=iterations)
	cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if len(cnts) == 2 else cnts[1]
	for c in cnts:
		cv2.drawContours(image, [c], -1, (255,255,255), draw_size)
	vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,kernel_size))
	detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=iterations)
	cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if len(cnts) == 2 else cnts[1]
	for c in cnts:
		cv2.drawContours(image, [c], -1, (255,255,255), draw_size)
	return image


def correct_skew(image, delta=1, limit=5):
	try:
		def determine_score(arr, angle):
			data = inter.rotate(arr, angle, reshape=False, order=0)
			histogram = np.sum(data, axis=1)
			score = np.sum((histogram[1:] - histogram[:-1]) ** 2)
			return histogram, score
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
		scores = []
		angles = np.arange(-limit, limit + delta, delta)
		for angle in angles:
			histogram, score = determine_score(thresh, angle)
			scores.append(score)
		best_angle = angles[scores.index(max(scores))]
		(h, w) = image.shape[:2]
		center = (w // 2, h // 2)
		M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
		rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, \
				  borderMode=cv2.BORDER_REPLICATE)
		return best_angle, rotated
	except Exception as e:
		print("correct_skew Exception: " + str(e))


def get_doc_type(local = False):
	last_mod = 0
	data_path = 'types.json'
	if os.path.exists(data_path):
		last_mod = os.path.getmtime(data_path)
	if time.time() - last_mod > 300 and local == False: # 5 mins
		try:
			payload = { "Account": "service.api@vps.com.vn", "Password": "service@2020!" }
			#local to sever
			r = requests.post("http://localhost:8054/api/users/authenticateService", json=payload)
			if r.ok:
				headers = {'authorization': f"Bearer {r.json()['token']}"}
				r = requests.get("http://localhost:8054/api/documentTypes/listTypes", headers=headers)
				data = r.json()
				f = open(data_path, 'w', encoding='utf-8')
				f.write(r.text)
				return data
			else:
				return { "error": r.content }
		except:
			return json.load(open(data_path, 'r', encoding='utf-8'))
	else:
		return json.load(open(data_path, 'r', encoding='utf-8'))

ner_custom_whitelist = ["giay_yc_rut_tien_ky_quy_ck_ps_qua_dt",
"giay_yc_nop_tien_ky_quy_ck_ps_qua_dt",
"giay_yc_chuyen_khoan_qua_dt",
"giay_yc_chuyen_khoan_truc_tuyen",
"giay_yc_chuyen_khoan_noi_bo_tai_quay",
"giay_yc_rut_tien_ky_quy_ck_ps_tai_quay",
"giay_yc_nop_tien_ky_quy_ck_ps_tai_quay"]

host_tess_list = ["10.32.32.31","10.32.32.31","10.32.32.30","localhost","localhost"]

app = Flask(__name__)

local = '0'
if len(sys.argv) > 1:
	print(sys.argv[1])
	local = sys.argv[1]
	print(local)
else:
	local = '0'

dir_path = os.path.dirname(os.path.realpath(__file__))
print(dir_path, flush=True)
dp = dir_path.split("/")

#local to sever
if local != "0":
	directory = "/home/vpsadmin/docker/doc-data/processing/"
else:
	directory = "/mnt/docs/processing/"

print(directory, flush=True)
types = {}
json_obj = json.load(open('types.json', 'r', encoding='utf-8'))
for d in json_obj:
	types[d['name']] = d['keywords']

print("Sever started........", flush=True)

@app.route("/doc_dvkh", methods=["POST"])
def doc():
	try:
		now = str(datetime.datetime.now().timestamp()).split(".")[0]
		department = request.form["department"].lower()[:4]
		print(department, flush=True)
		directory_temp = directory + "temp/"
		directory_saved =  directory + "saved/"
		image = request.files['image']
		print(request.files['image'], flush=True)
		print(image, flush=True)
		filepath = directory_temp + now + "_" + str(image.filename)
		filepath = filepath.replace(" ","").replace(":","").replace(".","_")
		filepath_saved = directory_saved + now + "___" + str(image.filename)
		filepath_saved = filepath_saved.replace(" ","").replace(":","").replace(".","_")
		Path(filepath).mkdir(parents=True, exist_ok=True)
		Path(filepath_saved).mkdir(parents=True, exist_ok=True)
		image.save(filepath + "/file.pdf")
		f = filepath + "/file.pdf"
		file_extension = ".jpg"

		try:
			try:
				print("Split PDF 0")
				imgs = convert_from_path(f, dpi=150, thread_count=10, output_folder=filepath + "/", grayscale=False, fmt='jpeg')

				# first attemp
				onlyfiles = [f for f in listdir(filepath + "/") if isfile(join(filepath + "/", f))]
				for file in onlyfiles:
					if file.count("-") == 5:
						if ( os.path.getsize(filepath + "/" + file) / (1024*1024) ) < 1:
							# delete all
							print("small filesize delte all and upgrade DPI")
							for file in onlyfiles:
								if file.count("-") == 5:
									os.remove(filepath + "/" + file)
							# and upgrade dpi
							imgs = convert_from_path(f, dpi=300, thread_count=10, output_folder=filepath + "/", grayscale=False, fmt='jpeg')
						break

				# second attemp
				onlyfiles = [f for f in listdir(filepath + "/") if isfile(join(filepath + "/", f))]
				prefix = ""
				for file in onlyfiles:
					if file.count("-") == 5:
						print(file)
						sp = file.split("-")
						prefix_cfp = sp[0] + "-" + sp[1] + "-" + sp[2] + "-" + sp[3] + "-" + sp[4]
						number = str(int(sp[5].replace(".jpg","")))
						temp = cv2.imread(filepath + "/" + file)
						cv2.imwrite(filepath + "/prefix-" + number + ".jpg", temp)
						os.remove(filepath + "/" + file)
				print(prefix, flush=True)
			except Exception as e:
				print(e)
				print("Exception PDF")
				print(gotoException)
				# return {"result" : "OK", "data": [], "code": 200,"path_rotated": "" , "path": ""}
		except Exception as e:
			try:
				onlyfiles = [f for f in listdir(filepath + "/") if isfile(join(filepath + "/", f))]
				print(onlyfiles)
				print("Removing Files")
				for file in onlyfiles:
					if ".jpg" in file:
						print(file)
						os.remove(filepath + "/" + file)
				print("Split PDF 1 . Third attemp")
				flag = fast_extract_image_pdf(f,filepath)
			except Exception as e:
				print("Exception PDF")
				return {"result" : "OK", "data": [], "code": 200,"path_rotated": "" , "path": ""}

		onlyfiles = [f for f in listdir(filepath + "/") if isfile(join(filepath + "/", f))]
		print("Removing Files")
		counter=0
		for file in onlyfiles:
			if (not "prefix" in file) and (not ".pdf" in file):
				print(file)
				os.remove(filepath + "/" + file)
			else:
				counter+=1

		print(onlyfiles)

		prefix = "prefix"
		prefix_saved = "prefix-saved"

		threshold_list = {}
		last_page = 0
		last_label = ""

		#prepare data for pool calling for rotating multiprocessing
		print("rotateMulti")
		print(counter)

		inputs = []
		for i in range(1,counter):
			try:
				# Preprocess Image : Remove Box and Skew
				fname = filepath + "/" + prefix + "-" + str(i) + file_extension
				fname_saved = filepath + "/" + prefix_saved + "-" + str(i) + file_extension
				inputs.append((fname,fname_saved))
			except Exception as e:
				print(e, flush=True)


		pool = mp.Pool(processes=10)
		results = pool.map(rotateMulti, inputs)

		print("rotateOriginalFile")
		f_rot = f.replace("file.pdf","file-rot.pdf")

		print(f,f_rot)

		pdf_in = open(f, 'rb')
		pdf_reader = PdfFileReader(pdf_in)
		pdf_writer = PdfFileWriter()
		for pagenum in range(pdf_reader.numPages):
			page = pdf_reader.getPage(pagenum)
			print(pagenum)
			page.rotateCounterClockwise(results[pagenum])
			pdf_writer.addPage(page)
		pdf_out = open(f_rot, 'wb')
		pdf_writer.write(pdf_out)
		pdf_out.close()
		pdf_in.close()

		os.remove(f)
		os.renames(f_rot,f)

		print("sendImageBasedClassify")

		#prepare data for pool calling for imaged_based multiprocessing
		inputs = []
		for i in range(1,counter):
			try:
				name_send = prefix + "-" + str(i) + file_extension
				fname = filepath + "/" + prefix_saved + "-" + str(i) + file_extension
				inputs.append((name_send, fname, department))
			except Exception as e:
				print(e, flush=True)

		pool = mp.Pool(processes=10)
		results = pool.map(sendImageBasedClassify, inputs)


		print("sendTesseractBasedClassify")
		#prepare data for pool calling for tesseract_based_call_Hung multiprocessing
		inputs = []
		tag_dict = {}
		for i in range(1,counter):
			try:
				label = results[i-1]
				fname = filepath + "/" + prefix + "-" + str(i) + file_extension
				name_send = prefix + "-" + str(i) + file_extension
				inputs.append((label,fname,department,name_send,i))
			except Exception as e:
				print(e, flush=True)

		pool = mp.Pool(processes=10)
		results = pool.map(sendTesseractBasedClassify, inputs)

		print("Processing After sendTesseractBasedClassify")

		results_ = [x[0] for x in results]
		tag_results = [x[2] for x in results]
		doc_list = []
		for i in range(1,counter):
			try:
				label = results_[i-1]
				if(label != "unk"):
					doc_list.append((last_label,last_page,i-1))
					last_page = i
					last_label = label
				if(counter == 1):
					last_label = label
			except Exception as e:
				print(e, flush=True)
		# append last page
		doc_list.append((last_label,last_page,i-1))
		last_page = i
		last_label = label
		doc_dict = {}
		for dl in doc_list:
			if(dl[0] != ""):
				doc_dict[dl[1]] = (dl[0],dl[2],tag_results[dl[1]-1])
		number_to_tess = [x[1] for x in doc_list]

		print(doc_list, flush=True)
		print("number_to_tess  : " + str(number_to_tess), flush=True)
		print(doc_dict)

		print("Saving rotated Parts")
		with open(filepath + "/file1.pdf", "wb") as f:
			names = []
			for i in range(1,counter):
				try:
					fname = filepath + "/" + prefix_saved + "-" + str(i) + file_extension
					print(fname)
					names.append(fname)
				except Exception as e:
					print(e)
			f.write(img2pdf.convert(names))

		print("Saving Splited Parts")
		for i in range(1,counter):
			try:
				docType = doc_dict[i][0]
				print(docType)
				start = i
				end = doc_dict[i][1] + 1
				# if(end == counter - 2 and start == counter - 3):
				# 	end = counter -1
				if(start == end):
					end = start + 1
				print("start end: " + str(start) + "-" + str(end))
				with open(filepath_saved + "/" +docType + "-" + str(start) + "-" + str(end) + ".pdf", "wb") as f:
					print(filepath_saved + "/" +docType + "-" + str(start) + "-" + str(end) + ".pdf")
					names = []
					for i in range(start,end):
						fname = filepath + "/" + prefix_saved + "-" + str(i) + file_extension
						print(fname)
						names.append(fname)
					print(names)
					f.write(img2pdf.convert(names))
			except Exception as e:
				print("Exception: " + str(e))

		print("Tag final:")
		tag_final = []
		for i in range(1,counter):
			if( i in number_to_tess):
				res = doc_dict[i][2]
				start = i
				end = doc_dict[i][1] + 1
				# if(end == counter - 2 and start == counter - 3):
				# 	end = counter -1
				if(start == end):
					end = start + 1
				res["pages"] = [start,end]
				res["docType"] = doc_dict[i][0]
				tag_final.append(res)


		path_rotated = ""
		path_rotated = "processing/" + filepath.replace(directory,"") + "/file1.pdf"

		print(str({"result" : "OK", "data": tag_final, "code": 200,"path_rotated": path_rotated , "path": "processing/" + filepath_saved.replace(directory,"")}))
		return {"result" : "OK", "data": tag_final, "code": 200,"path_rotated": path_rotated , "path": "processing/" + filepath_saved.replace(directory,"")}
	except Exception as e:
		print(e, flush=True)
		return {"result" : "Error", "data": str(e), "code": 500}

if __name__ == '__main__':
	import logging
	logFormatStr = '[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
	logging.basicConfig(format = logFormatStr, filename = "global.log", level=logging.DEBUG)
	formatter = logging.Formatter(logFormatStr,'%m-%d %H:%M:%S')
	fileHandler = logging.FileHandler("summary.log")
	fileHandler.setLevel(logging.DEBUG)
	fileHandler.setFormatter(formatter)
	streamHandler = logging.StreamHandler()
	streamHandler.setLevel(logging.DEBUG)
	streamHandler.setFormatter(formatter)
	app.logger.addHandler(fileHandler)
	app.logger.addHandler(streamHandler)
	app.logger.info("Logging is set up.")
	app.run(host='0.0.0.0', port=8000, threaded=False, processes=5)
	# http_server = WSGIServer(('0.0.0.0', 8000), app)
	# http_server.serve_forever()
