#coding=utf-8
from flask import Flask, url_for
from flask import request
from flask import json
from flask import Response
from flask import jsonify
from gevent.pywsgi import WSGIServer
import requests

from PIL import Image
import cv2

from scipy.ndimage import interpolation as inter
import pytesseract
from pytesseract import image_to_string, image_to_boxes
from pytesseract import Output
from pdf2image import convert_from_path
import img2pdf
from PyPDF2 import PdfFileWriter, PdfFileReader

from scipy import ndimage
import numpy as np

import io
import os
os.environ['OMP_THREAD_LIMIT'] = '10'
from os import listdir
from os.path import isfile, join
import sys
import re

import unidecode
import ntpath
from shutil import rmtree
from pathlib import Path
import json
import time
import datetime

import concurrent.futures
from fuzzysearch import find_near_matches

import string
import random

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def get_match_line(lines, signal, min, dist=1):
    if signal == None:
        return ''
    signal = unidecode.unidecode(signal).lower()
    data = ''
    for line in lines:
        line_decode = unidecode.unidecode(line).lower()
        match = find_near_matches(signal, line_decode, max_l_dist=dist)
        if len(match) > 0:
            data = line[match[0].end:]
            if len(data) >= min:
                break
    if len(data) < min:
        data = ''

    return data

def get_account_number(lines, signal):
    data = get_match_line(lines, signal, 10, dist=5)
    last_char = ''
    result = ''
    for i in range(0, len(data)):
        this_char = data[i]
        if this_char == 'c' or this_char == 'C':
            result += this_char
        elif this_char.isdigit():
            result += this_char
        last_char = this_char
    return result
    data = get_match_line(lines, signal, 9)
    return data

def get_number(lines, signal, min, max):
    data = get_match_line(lines, signal, min)
    x = re.search(rf"\b[\d-]{{{min},{max}}}", data)
    if x != None:
        return x.group()
    return ''

def get_name(lines, signal):
    data = get_match_line(lines, signal, 6)
    x = re.search(r"\b[\w\s]{5,60}(?=[ ]{2}|$)", data)
    if x != None:
        return x.group()
    return ''

def get_date(lines, signal):
    data = get_match_line(lines, signal, min=10)
    decode_data = unidecode.unidecode(data)
    x = re.search(r"\b(?:(\d{2})/(\d{2})/(\d{4}))|(?:(\d{2})\sthang\s(\d{2})\snam\s(\d{4}))", decode_data)
    if x != None:
        if x.group(1) != None:
            return f'{x.group(1)}/{x.group(2)}/{x.group(3)}'
        else:
            return f'{x.group(4)}/{x.group(5)}/{x.group(6)}'
    return ''

def get_address(lines, signal):
    data = get_match_line(lines, signal, 6)
    decode_data = unidecode.unidecode(data)
    x = re.search(r"\b[\w][\w/,\s]{4,}[\w](?=[ ]{2}|$)", decode_data)
    if x != None:
        return data[x.span()[0]:x.span()[1]].strip(',.-| ')
    return ''

def get_id_location(lines, signal):
    data = get_match_line(lines, signal, 6)
    decode_data = get_alphanumeric(unidecode.unidecode(data), repl=' ')
    x = re.search(r"\S+(\s\S+)*", decode_data)
    if x != None:
        return data[x.span()[0]:x.span()[1]]
    return ''

def get_email(lines, signal):
    data = get_match_line(lines, signal, 9)
    data = get_alphanumeric(unidecode.unidecode(data), '@.')
    x = re.search(r"\S+@\S+", data)
    if x != None:
        return data[x.span()[0]:x.span()[1]]
    return ''

def get_alphanumeric(raw, esc = None, repl = None):
    alphanumeric = ""
    for character in raw:
        if character.isalnum() or (esc != None and character in esc):
            alphanumeric += character
        elif repl != None:
            alphanumeric += repl
    return alphanumeric

def rotate(image, center = None, scale = 1):
    angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
    rotated = image
    if angle != 0 and angle != 360:
        rotated  = ndimage.rotate(image, angle)
    return rotated

def rm_contour(image,kernel_size,draw_size,iterations):
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size,1))
    detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=iterations)
    cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(image, [c], -1, (255,255,255), draw_size)
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,kernel_size))
    detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=iterations)
    cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(image, [c], -1, (255,255,255), draw_size)
    return image


def correct_skew(image, delta=1, limit=5):
    def determine_score(arr, angle):
        data = inter.rotate(arr, angle, reshape=False, order=0)
        histogram = np.sum(data, axis=1)
        score = np.sum((histogram[1:] - histogram[:-1]) ** 2)
        return histogram, score
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    scores = []
    angles = np.arange(-limit, limit + delta, delta)
    for angle in angles:
        histogram, score = determine_score(thresh, angle)
        scores.append(score)
    best_angle = angles[scores.index(max(scores))]
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, \
              borderMode=cv2.BORDER_REPLICATE)
    return best_angle, rotated

def get_doc_type(local = False):
    last_mod = 0
    data_path = 'types.json'
    if os.path.exists(data_path):
        last_mod = os.path.getmtime(data_path)
    if time.time() - last_mod > 300 and local == False: # 5 mins
        try:
            payload = { "Account": "service.api@vps.com.vn", "Password": "service@2020!" }
            #local to sever
            r = requests.post("http://localhost:8054/api/users/authenticateService", json=payload)
            if r.ok:
                headers = {'authorization': f"Bearer {r.json()['token']}"}
                r = requests.get("http://localhost:8054/api/documentTypes/listTypes", headers=headers)
                data = r.json()
                f = open(data_path, 'w', encoding='utf-8')
                f.write(r.text)
                return data
            else:
                return { "error": r.content }
        except:
            return json.load(open(data_path, 'r', encoding='utf-8'))
    else:
        return json.load(open(data_path, 'r', encoding='utf-8'))



app = Flask(__name__)

local = '0'
if len(sys.argv) > 1:
    print(sys.argv[1])
    local = sys.argv[1]
    print(local)
else:
    local = '0'

dir_path = os.path.dirname(os.path.realpath(__file__))
print(dir_path, flush=True)
dp = dir_path.split("/")

#local to sever
if local != "0":
    directory = "/home/vpsadmin/docker/doc-data/processing/"
else:
    directory = "/mnt/docs/processing/"

print(directory, flush=True)
types = {}
json_obj = json.load(open('types.json', 'r', encoding='utf-8'))
for d in json_obj:
    types[d['name']] = d['keywords']

print("Sever started........", flush=True)

@app.route("/doc_dvkh", methods=["POST"])
def doc():
    try:
        now = str(datetime.datetime.now().timestamp()).split(".")[0]
        department = request.form["department"].lower()
        print(department, flush=True)
        directory_temp = directory + "temp/"
        directory_saved =  directory + "saved/"
        image = request.files['image']
        print(request.files['image'], flush=True)
        print(image, flush=True)
        filepath = directory_temp + now + "_" + str(image.filename)
        filepath = filepath.replace(" ","").replace(":","").replace(".","_")
        filepath_saved = directory_saved + now + "___" + str(image.filename)
        filepath_saved = filepath_saved.replace(" ","").replace(":","").replace(".","_")
        Path(filepath).mkdir(parents=True, exist_ok=True)
        Path(filepath_saved).mkdir(parents=True, exist_ok=True)
        image.save(filepath + "/file.pdf")
        f = filepath + "/file.pdf"
        # split PDF to detect rotation angle
        # imgs = convert_from_path(f, dpi=300, output_folder=filepath + "/", grayscale=False, fmt='jpeg')
        imgs = convert_from_path(f, dpi=300)

        i = 0
        for img in imgs:
            i+=1
            img.save(filepath+"/prefix-"+str(i)+".jpg", "JPEG")
            print(filepath+"/prefix-"+str(i))

        # retrieve prefix of images
        onlyfiles = [f for f in listdir(filepath + "/") if isfile(join(filepath + "/", f))]
        prefix = ""
        for file in onlyfiles:
            if file.count("-") == 5:
                sp = file.split("-")
                prefix = sp[0] + "-" + sp[1] + "-" + sp[2] + "-" + sp[3] + "-" + sp[4]
        print(prefix, flush=True)

        prefix = "prefix"
        doc_list = []
        counter = len(onlyfiles) + 1
        print(counter, flush=True)

        num_zero = 1

        threshold_list = {}
        last_page = 0
        last_label = ""

        for i in range(1,counter):
            try:
                fname = filepath + "/" + prefix + "-" + str(i).zfill(num_zero) + ".jpg"
                image = Image.open(fname)
                angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
                rotated = image
                if angle != 0 and angle != 360:
                    print(angle)
                    rotated  = ndimage.rotate(image, angle)
                cv2.imwrite(fname,rotated)
                imgs[i-1] = Image.fromarray(rotated)
            except Exception as e:
                print(e, flush=True)

        with concurrent.futures.ProcessPoolExecutor(max_workers=16) as executor:
            for i in range(1,counter):
                try:
                    fname = filepath + "/" + prefix + "-" + str(i).zfill(num_zero) + ".jpg"
                    print(fname , flush=True)
                    url = "http://localhost:3002/recognize"
                    files = {'image': (prefix + "-" + str(i).zfill(num_zero) + ".jpg", open(fname, 'rb')),}
                    payload = {'department' : department}
                    label = "unk"
                    try:
                        response = requests.post(url, files = files, data = payload)
                        label = response.json()["details"][0]["label"]
                        print(response.json(),flush=True)
                    except Exception as e:
                        print(e, flush=True)
                        label = "unrecognize"
                    if(label == "unrecognize"):
                        try:
                            print("call Hung", flush=True)
                            print("test_print")
                            #local to sever
                            url_ = "http://api.internal.net:8889/api/v1/recognize/text"
                            if local != "0":
                                url_ = "http://10.32.37.8:8889/api/v1/recognize/text"
                            custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1'
                            text = ""
                            try:
                                text = pytesseract.image_to_string(Image.open(fname),config=custom_config, lang='eng+vie')
                                print(text)
                            except Exception as e:
                                print("tesseract error: " + str(e), flush=True)
                            payload = {'department': department, 'text': text}
                            response = requests.post(url_, json = payload)
                            print(response.json(), flush=True)
                            label = response.json()['data']['type']
                        except Exception as e:
                            print(e, flush=True)
                    threshold_list[label] = (i)
                    if(label != "unk"):
                        doc_list.append((last_label,last_page,i-1))
                        last_page = i
                        last_label = label
                    if(counter == 1):
                        last_label = label
                    print(response.json(), flush=True)
                except Exception as e:
                    print(e, flush=True)
        # append last page
        doc_list.append((last_label,last_page,i-1))
        last_page = i
        last_label = label
        print(doc_list, flush=True)
        doc_dict = {}
        for dl in doc_list:
            if(dl[0] != ""):
                doc_dict[dl[1]] = (dl[0],dl[2])
        number_to_tess = [x[1] for x in doc_list]
        print("number_to_tess  : " + str(number_to_tess), flush=True)
        total_text = []
        result = []
        #write rotated final pdf
        print("Pre write")
        with open(filepath + "/file1.pdf", "wb") as f:
            names = []
            for i in range(1,counter-1):
                try:
                    fname = filepath + "/" + prefix + "-" + str(i).zfill(num_zero) + ".jpg"
                    print(fname)
                    names.append(fname)
                except Exception as e:
                    print(e)
            f.write(img2pdf.convert(names))

        print("after write")
        text_acc = ""
        with concurrent.futures.ProcessPoolExecutor(max_workers=16) as executor:
            for i in range(1,counter+1):
                try:
                    if( i in number_to_tess):
                        fname = filepath + "/" + prefix + "-" + str(i).zfill(num_zero) + ".jpg"
                        print(fname, flush=True)
                        custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1 -l vie'
                        text = ""

                        # params = (('input', text),)
                        # response = requests.get('http://localhost:5000/api/', params=params)
                        # print(response.json())
                        # applying custom model for account_number
                        image = np.array(imgs[i-1]) # cv2_image
                        angle_, rotated = correct_skew(image)
                        image = rotated
                        height, width, channels = image.shape
                        half_image = 0
                        if width > height:
                            image = cv2.resize(image, (0,0), fx=2480/width, fy=1750/height)
                            half_image = 1
                        else:
                            image = cv2.resize(image, (0,0), fx=2480/width, fy=3500/height)
                        if half_image:
                            print("half_image")
                            image = rm_contour(image,5,6,10)
                        else:
                            image = rm_contour(image,6,8,10)

                        img = Image.fromarray(image)

                        try:
                            text = pytesseract.image_to_string(img,config=custom_config)
                            # print(text)
                        except Exception as e:
                            print("tesseract error: " + str(e), flush=True)

                        lines = text.split('\n')
                        json_obj = get_doc_type()
                        for d in json_obj:
                            types[d['name']] = d['keywords']
                        keywords = types[doc_dict[i][0]]
                        print(keywords)
                        res = {}
                        res["content"] = text
                        # res["content_acc"] = text_acc
                        res["docType"] = doc_dict[i][0]
                        res["pages"] = [i,doc_dict[i][1]+1]
                        tags = {}
                        for keyword in keywords:
                            if 'TEXT_IN_BOX' in keyword['data_Type'] or 'ACC' in keyword["name"]:
                                print(f'{keyword["description"]}: ' + get_account_number(lines, keyword['match_Phrase']))
                                tags[keyword["name"]] = get_account_number(lines, keyword['match_Phrase'])
                                if 'TEXT_IN_BOX' in keyword['data_Type']:
                                    print("tesseracting")
                                    custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1 -l fdm-v12'
                                    text_acc = ""
                                    account_num = ""
                                    try:
                                        text_acc = pytesseract.image_to_string(img,config=custom_config).lower().replace(" ","")
                                        start = text_acc.lower().find('026c')
                                        # print(text_acc)
                                        if(start > 0):
                                            account_num = text_acc[start:start + 10]
                                            account_num = ''.join(e for e in account_num if e.isalnum())
                                            print(account_num, flush=True)
                                    except Exception as e:
                                        print("tesseract error: " + str(e), flush=True)
                                    tags[keyword["name"]] = account_num.upper()
                            elif keyword['data_Type'] == 'NAME':
                                print(f'{keyword["description"]}: ' + get_name(lines, keyword['match_Phrase']))
                                tags[keyword["name"]] = get_name(lines, keyword['match_Phrase'])
                            elif keyword['data_Type'] == 'NUMBER':
                                print(f'{keyword["description"]}: ' + get_number(lines, keyword['match_Phrase'], 9, 20))
                                tags[keyword["name"]] = get_number(lines, keyword['match_Phrase'], 9, 20)
                            elif keyword['data_Type'] == 'ADDRESS':
                                if keyword['name'] == 'ID_LOCATION':
                                    print(f'{keyword["description"]}: ' + get_id_location(lines, keyword['match_Phrase']))
                                    tags[keyword["name"]] = get_id_location(lines, keyword['match_Phrase'])
                                else:
                                    print(f'{keyword["description"]}: ' + get_address(lines, keyword['match_Phrase']))
                                    tags[keyword["name"]] = get_address(lines, keyword['match_Phrase'])
                            elif keyword['data_Type'] == 'DATE':
                                print(f'{keyword["description"]}: ' + get_date(lines, keyword['match_Phrase']))
                                tags[keyword["name"]] = get_date(lines, keyword['match_Phrase'])
                            elif keyword['data_Type'] == 'EMAIL':
                                print(f'{keyword["description"]}: ' + get_email(lines, keyword['match_Phrase']))
                                tags[keyword["name"]] = get_email(lines, keyword['match_Phrase'])

                        res["tags"] = tags
                        result.append(res)
                        docType = res["docType"]
                        start = i
                        end = doc_dict[i][1] + 1
                        if(end == counter + 1):
                            end = counter

                        with open("/mnt/docs/training" +"/" + docType + "-" + str(start) + "-" + str(end) + "--NER.txt", "w") as f:
                            f.write(text)
                        with open(filepath_saved + "/" +docType + "-" + str(start) + "-" + str(end) + ".pdf", "wb") as f:
                            names = []
                            for i in range(start,end):
                                fname = filepath + "/" + prefix + "-" + str(i).zfill(num_zero) + ".jpg"
                                names.append(fname)
                            f.write(img2pdf.convert(names))
                        # Implementing cut tesseract line lv4
                        custom_config = r'-c preserve_interword_spaces=1 --oem 1 --psm 1 -l vie'
                        print("tesseracting")
                        d = pytesseract.image_to_data(img, output_type=Output.DICT, config = custom_config)
                        n_boxes = len(d['level'])
                        # print(d)
                        temp = ""
                        conf_n = 0
                        conf = 0
                        img = image
                        prev_lv4_data = (1,1,1,1)
                        # for i in range(n_boxes):
                        #     try:
                        #         if(d['level'][i] == 4):
                        #             current_lv4_text = temp
                        #             conf = conf / conf_n
                        #             # Checking if keyword in a line?
                        #             for keyword in keywords:
                        #                 if(unidecode.unidecode(keyword["match_Phrase"]).lower() in unidecode.unidecode(current_lv4_text).lower()):
                        #                     print(current_lv4_text)
                        #                     # print(current_lv4_text)
                        #                     (x, y, w, h) = prev_lv4_data
                        #                     crop_img = img[y:y+h, x:x+w]
                        #                     # directory ???
                        #                     name = str(id_generator(22))
                        #                     with open(filepath_saved + + name + ".txt", "w") as text_file:
                        #                         text_file.write(current_lv4_text)
                        #                     cv2.imwrite(filepath_saved + name + ".png", crop_img)
                        #             temp = ""
                        #             conf_n = 0
                        #             conf = 0
                        #             prev_lv4_data = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
                        #         elif(d['level'][i] == 5):
                        #             conf_n += 1
                        #             conf += d['conf'][i]
                        #             temp = temp + " " + d["text"][i]
                        #     except Exception as e:
                        #         print(e)

                except Exception as e:
                    print(e, flush=True)

        try:
            #rmtree(filepath)
            print("should delete", flush=True)
        except Exception as e:
            print("rmtree: " + str(e), flush=True)

        path_rotated = ""
        path_rotated = "processing/" + filepath.replace(directory,"") + "/file1.pdf"

        print(str({"result" : "OK", "data": result, "code": 200,"path_rotated": path_rotated , "path": "processing/" + filepath_saved.replace(directory,""), "threshold": str(threshold_list)}))
        return {"result" : "OK", "data": result, "code": 200,"path_rotated": path_rotated , "path": "processing/" + filepath_saved.replace(directory,""), "threshold": str(threshold_list)}
    except Exception as e:
        print(e, flush=True)
        return {"result" : "Error", "data": str(e), "code": 422}

if __name__ == '__main__':
    import logging
    logFormatStr = '[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
    logging.basicConfig(format = logFormatStr, filename = "global.log", level=logging.DEBUG)
    formatter = logging.Formatter(logFormatStr,'%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler("summary.log")
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.DEBUG)
    streamHandler.setFormatter(formatter)
    app.logger.addHandler(fileHandler)
    app.logger.addHandler(streamHandler)
    app.logger.info("Logging is set up.")
    app.run(host='0.0.0.0', port=8002, threaded=True)
    # http_server = WSGIServer(('0.0.0.0', 8000), app)
    # http_server.serve_forever()
