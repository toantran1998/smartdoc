#coding=utf-8
from flask import Flask, url_for
from flask import request
from flask import json
from flask import Response
from flask import jsonify

import pymysql
import datetime

myConnection = pymysql.connect(host="10.32.37.5", port = 33060, user="root", passwd="fdm", db="smartdoc")
cursor = myConnection.cursor()

app = Flask(__name__)

print("Sever Logging started........", flush=True)

@app.route("/log", methods=["POST"])
def doc():
    try:
        log = request.json["log"]
        print(log)
        log[1] = log[1][:19]
        log[0] = log[0].replace("/mnt/docs/processing/temp/","")
        log[3] = log[3][:100]
        
        cursor.execute("INSERT INTO log (name,time,status,note,fullog) VALUES (%s, %s, %s,%s, %s)",(log[0],log[1],log[2],log[3],log[4]))
        myConnection.commit()

        return {"result" : "1", "data": "1", "code": 200}
    except Exception as e:
        print(e, flush=True)
        return {"result" : "Error", "data": str(e), "code": 422}

if __name__ == '__main__':
    import logging
    logFormatStr = '[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
    logging.basicConfig(format = logFormatStr, filename = "global.log", level=logging.DEBUG)
    formatter = logging.Formatter(logFormatStr,'%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler("summary.log")
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.DEBUG)
    streamHandler.setFormatter(formatter)
    app.logger.addHandler(fileHandler)
    app.logger.addHandler(streamHandler)
    app.logger.info("Logging is set up.")
    app.run(host='0.0.0.0', port=9595, threaded=False, processes=10)
    # http_server = WSGIServer(('0.0.0.0', 8000), app)
    # http_server.serve_forever()
