import requests
import concurrent.futures
from multiprocessing import Process
from multiprocessing import Pool
import os

from multiprocessing import Process
from multiprocessing import Semaphore
import time

def f(of, sema):
	ofs = of.split("/")
	ofl = len(ofs)
	off = ofs[ofl - 1]
	url = "http://10.32.37.5:8000/doc_dvkh"
	files = {'image': (off, open(of, 'rb')),}
	payload = {'department' : "dvkh"}
	response = requests.post(url, files = files, data = payload)
	# with open(of + ".txt", "w") as text_file:
	# 	text_file.write(str(response.json()["data"]))
	print(str(response.json()["data"]))
	sema.release()

if __name__ == '__main__':
	t0 = time.time()
	concurrency = 5
	sema = Semaphore(concurrency)
	all_processes = []
	mypath = "/mnt/docs/DVKH/"
	temp = []
	for path, subdirs, files in os.walk(mypath):
		for name in files:
			try:
				of = os.path.join(path, name)
				print(of)
				sema.acquire()
				for i in range(1,6):
					p = Process(target=f, args=(of, sema))
				all_processes.append(p)
				p.start()
			except Exception as e:
				print(e, flush=True)
	for p in all_processes:
		p.join()
	t1 = time.time()
	print(t1-t0)
