import json
import glob
from fuzzywuzzy import fuzz
import copy

import time

recorrect_files = glob.glob("/home/ducnh/Downloads/lab/doc_server/wrong/*.json")
recorrect_dict = {}
recorrect_dict["match"] = []
recorrect_dict["god_label"] = []

for rf in recorrect_files:
    with open(rf) as handle:
        dictdump = json.loads(handle.read())
        recorrect_dict["match"].append(dictdump["match"])
        dictdump["god_label"]["lines"] = dictdump["lines"]
        recorrect_dict["god_label"].append(dictdump["god_label"])

doc_type_dict = {}
with open("/home/ducnh/Downloads/lab/doc_server/doc_key.json") as handle:
    doc_type_dict = json.loads(handle.read())

#Trim dictionary
# for deps, depps in doc_type_dict.items():
#     for doc_type, doc_l in depps:


old_doc = copy.deepcopy(doc_type_dict)
index_processed = set()

for item in recorrect_dict["match"]:
    title = item["title"]
    debug = item["debug"]["total_line"]
    dep = item["debug"]["department"]
    samples = []
    for line in debug:
        samples.append(line[0])
        samples.append(line[1])
    clone = doc_type_dict[dep][title]
    doc_type_dict[dep][title] = [x for x in clone if x not in samples]
    index_processed.add((title,dep))

for item in recorrect_dict["god_label"]:
    title = item["title"]
    debug = item["debug"]["total_line"]
    dep = item["debug"]["department"]
    samples = []
    for line in debug:
        samples.append((line[0],0))
        samples.append((line[1],0))
    samples = samples + item["lines"]
    counter = 0
    list_of_added = []
    samples.sort(key=lambda s: len(s),reverse=True)
    for itemline in samples[:7]:
        instance = itemline[0]
        flag = 0
        for kco, vco in doc_type_dict[dep].items():
            if not kco == title:
                for vco_line in vco:
                    # do comparision fuzzywuzzy
                    comparision_score = fuzz.ratio(vco_line,instance)
                    if comparision_score > 90:
                        counter += 1
                        flag = 1
                        break
        if flag == 0 and len(instance) > 40:
            list_of_added.append(instance)
    for itemline in samples:
        instance = itemline[0]
        flag = 0
        for kco, vco in doc_type_dict[dep].items():
            if kco == title:
                for vco_line in vco:
                    # do comparision fuzzywuzzy
                    comparision_score = fuzz.ratio(vco_line,instance)
                    if comparision_score > 95:
                        counter += 1
                        flag = 1
                        break
        if flag == 1:
            list_of_added.append(instance)
            # print(instance)
    list_of_added.sort(key=lambda s: len(s),reverse=True)
    # length_of_taken = len(doc_type_dict[dep][title])
    try:
        for adding in list_of_added:
            if (len(adding) > 20):
                doc_type_dict[dep][title].append(adding)
        list_of_post_added = doc_type_dict[dep][title]
        list_of_post_added.sort(key=lambda s: len(s),reverse=True)
        doc_type_dict[dep][title] = list_of_post_added
    except Exception as e:
        print("Exception: " + str(e))

    # Post Processing
    samples = doc_type_dict[dep][title]
    counter = 0
    list_of_added = []
    for itemline in samples:
        instance = itemline[0]
        flag = 0
        for kco, vco in doc_type_dict[dep].items():
            if not kco == title:
                for vco_line in vco:
                    # do comparision fuzzywuzzy
                    comparision_score = fuzz.ratio(vco_line,instance)
                    if comparision_score > 75:
                        counter += 1
                        flag = 1
                        break
        if flag == 1 and len(instance) > 40:
            list_of_added.append(instance)
    # print(list_of_added)
    for lof in list_of_added:
        # print(lof)
        print("Removeeeee")
        doc_type_dict[dep][title].remove(lof)

    # Self-check right away
    doc_key_dep = doc_type_dict[dep]
    fuzzy_scores = {}
    for key, value in doc_key_dep.items():
        total_score = 0
        counter = 0
        total_line = []
        for v in value:
            scores = []
            for lin in item["lines"]:
                vv = lin[0]
                cap = lin[1]
                if len(vv) > 20:
                    scores.append((v,vv,fuzz.ratio(v,vv)))
            scores.sort(key=lambda tup: tup[2],reverse=True)
            best_score = scores[0][2]
            threshold_scores = {"dvkh": 90,"ktgd":50,"ktnb":90,"dvtc":90}
            if(best_score > threshold_scores[dep]):
                counter += 1
                total_line.append(scores[0])
                total_score += best_score + counter
                if best_score > 98:
                    total_score += 500
        if(counter == 0):
            fuzzy_scores[key] = {"score":0,"total_line":[],"department":dep}
        else:
            total_score -= (10-counter)
            fuzzy_scores[key] = {"score":total_score,"total_line":total_line,"department":dep}
    top_match = {}
    match = sorted(fuzzy_scores,key=lambda k: fuzzy_scores[k]["score"],reverse=True)[0]
    if match == title:
        print("CORRECT")
        print(match)
        print(title)
    else:
        print("WRONGNESS")
        print(match)
        print(title)
        print(fuzzy_scores[match])
        print(fuzzy_scores[title])

for ip in index_processed:
    print(ip[0])
    print(len(doc_type_dict[ip[1]][ip[0]]))
    print(len(old_doc[ip[1]][ip[0]]))

with open('doc_type.json', 'w', encoding='utf-8') as f:
    json.dump(doc_type_dict, f, ensure_ascii=False, indent=4)

# import json
# doc_type_dict = {}
# with open("/home/ducnh/Downloads/lab/doc_server/types.json") as handle:
#     doc_type_dict = json.loads(handle.read())
#
# counter = 0
# for dtd in doc_type_dict:
#     print(dtd["description"])
