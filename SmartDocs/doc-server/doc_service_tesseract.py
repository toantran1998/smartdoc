#coding=utf-8
from flask import Flask, url_for
from flask import request
from flask import json
from flask import Response
from flask import jsonify
from gevent.pywsgi import WSGIServer
import requests

from PIL import Image
import cv2

from scipy.ndimage import interpolation as inter
import pytesseract
from pytesseract import image_to_string, image_to_boxes
from pytesseract import Output

from scipy import ndimage
import numpy as np

import io
import os
os.environ['OMP_THREAD_LIMIT'] = '1'
from os import listdir
from os.path import isfile, join
import sys
import re

import string
import random

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

app = Flask(__name__)

print("Sever Tesseract started........", flush=True)

@app.route("/tess_string", methods=["POST"])
def doc():
    try:
        image = request.files['image']
        print(request.files['image'], flush=True)
        print(image, flush=True)
        fname = "./temp/"+id_generator(22)+".jpg"
        image.save(fname)
        config = request.form["config"]
        text = ""
        try:
            print(config)
            text = pytesseract.image_to_string(Image.open(fname),config=config)
            # print(text)
        except Exception as e:
            print("tesseract error: " + str(e), flush=True)
            text = "tesseract error: " + str(e)

        os.remove(fname)
        return {"result" : "Success", "data": text, "code": 200}
    except Exception as e:
        print(e, flush=True)
        return {"result" : "Error", "data": str(e), "code": 422}

@app.route("/tess_data", methods=["POST"])
def doc_():
    try:
        image = request.files['image']
        print(request.files['image'], flush=True)
        print(image, flush=True)
        fname = "./temp/"+id_generator(22)+".jpg"
        image.save(fname)
        config = request.form["config"]
        d = {}
        try:
            print(config)
            d = pytesseract.image_to_data(Image.open(fname), output_type=Output.DICT, config = config)
            # print(text)
        except Exception as e:
            print("tesseract error: " + str(e), flush=True)
            text = "tesseract error: " + str(e)

        os.remove(fname)
        return {"result" : "Success", "data": d, "code": 200}
    except Exception as e:
        print(e, flush=True)
        return {"result" : "Error", "data": str(e), "code": 422}

if __name__ == '__main__':
    import logging
    logFormatStr = '[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
    logging.basicConfig(format = logFormatStr, filename = "global.log", level=logging.DEBUG)
    formatter = logging.Formatter(logFormatStr,'%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler("summary.log")
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.DEBUG)
    streamHandler.setFormatter(formatter)
    app.logger.addHandler(fileHandler)
    app.logger.addHandler(streamHandler)
    app.logger.info("Logging is set up.")
    app.run(host='0.0.0.0', port=2210, threaded=True)
    # http_server = WSGIServer(('0.0.0.0', 8000), app)
    # http_server.serve_forever()
