# doc_service

Dịch vụ phân loại văn bản và tự động nhận diện.

Câu lệnh để chạy service: nohup python3 ddoc_service.py > output.log &

Câu lệnh mẫu để thử service: curl -F image=@10.pdf 'http://localhost:9011/doc'

Input: file PDF (ver 0 đang hoạt động tốt trên các văn bản dịch vụ khách hàng)

Output: 

{"code":200,"data":[{"docType":"vps_contract","pages":[1,7],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"cmnd","pages":[7,8],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"de_nghi_cap_han_muc_bat_thuong","pages":[8,9],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_mo_tk_ky_quy","pages":[9,20],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"van_ban_chi_dinh_tk","pages":[20,21],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"xac_thuc_chu_ky_khach_hang","pages":[21,22],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_mb_ck","pages":[22,27],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_mb_ck","pages":[27,35],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_mo_tk_ck_phsinh","pages":[35,42],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_uq","pages":[42,45],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}},{"docType":"hd_vay","pages":[45,50],"tags":{"ACC_NUMBER":"026c063903","EMAIL":"lethuhong22@gmail.com","ID_LOCATION":" c\u1ee5c c\u00e1nh s\u00e1t","ID_NUMBER":"001161014566","PHONE_NUMBER":"0902983366","RELEASE_DATE":"15/08/2019"}}],"path":"processing/2020-01-17 17:05:44.502438+++3.pdf","result":"OK"}


{"code":422,"data":"list index out of range","result":"Error"}






